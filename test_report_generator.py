#!/usr/bin/env python

import unittest
import report_generator
import logging
import sys
import os

from datetime import datetime

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Mock Libraries ####################
#######################################

# Mock aws calls
class STS:
    '''
    Just a class for mock sts AWS calls for use in init tests.

    Later this will be something like:
        sts = STS()
    '''
    def __init__(self):
        self.account_number = '123456789012'

    def get_caller_identity(self):
        return {
            'Account': self.account_number
        }

class IAM:
    '''
    Just a class for mock iam AWS calls for use in init tests.

    Later this will be something like:
        iam = IAM(aliases=['unit_test'])
    '''

    def __init__(self, aliases=[]):
        self.aliases = aliases

    def list_account_aliases(self):
        return {
            "AccountAliases": self.aliases
        }

#######################################
### Unit Tests ########################
#######################################

class TestReportGenerator(unittest.TestCase):
    def test_log_json(self):
        print('')
        logger.info('Testing log_json()')

        # Test 1
        test_values = [
            {
                'unit': 'test'
            },
            '1,2'.split(',')
        ]

        for value in test_values:
            logger.info('Test case: {}'.format(str(value)))

            logger.info('Verifying that {} returns a string.'.format(value))
            log_value = report_generator.log_json(value)

            self.assertIsInstance(log_value, str)

        # Test 2
        test_values = [
            2,
            float(2),
            None,
            True,
            'String'
        ]

        for value in test_values:
            logger.info('Test case: {}'.format(str(value)))
            logger.info('Verifying that {} raised TypeError.'.format(value))
            with self.assertRaises(TypeError):
                report_generator.log_json(value)


    def test_get_aws_account(self):
        print('')
        logger.info('Testing get_aws_account()')

        iam = IAM()
        sts = STS()

        def test_alias(iam):
            account = report_generator.get_aws_account(iam=iam, sts=sts)
            logger.info('Account: {}'.format(account))

            return account


        self.assertEqual(test_alias(iam), sts.get_caller_identity()["Account"])

        iam = IAM(aliases=['unit_test'])

        self.assertEqual(test_alias(iam), iam.aliases[0])


    def test_get_time_period(self):
        print('')
        logger.info('Testing get_time_period()')

        time_period = report_generator.get_time_period(1)

        start       = time_period['Start']
        end         = time_period['End']

        # Test 1
        logger.info('Verifying that {time_period} is a dictionary'.format(time_period=time_period))

        self.assertIsInstance(time_period, dict)

        # Test 2
        def convert_date(date_string):
            format = r'%Y-%m-%d'
            converted_date = datetime.strptime(date_string, format)

            return converted_date

        converted_dates = [
            convert_date(date) for date in [start, end]
        ]

        logger.info('Verifying that {start} is before {end}'.format(start=start, end=end))

        self.assertGreater(converted_dates[1], converted_dates[0])


#######################################
### Execution #########################
#######################################

if __name__ == '__main__':
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    unittest.main()
