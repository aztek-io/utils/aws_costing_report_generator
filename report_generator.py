#!/usr/bin/env python
# Written by:   Robert J.
# Email:        robert@aztek.io

import logging
import sys
import boto3
import os
import json
import re

from datetime import timedelta, date
import matplotlib.pyplot as graph

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Boto3 Configs #####################
#######################################

cost_explorer   = boto3.client('ce')
sts             = boto3.client('sts')
iam             = boto3.client('iam')

#######################################
### Global Variables ##################
#######################################

TIME_PERIOD_IN_DAYS = os.environ.get('TIME_PERIOD_IN_DAYS', 14)

#######################################
### Main Function #####################
#######################################

def main(event, context):
    log_json(event, 'Event: {}')

    time_period = get_time_period()

    simplified_daily_usage = get_simplified_daily_usage(time_period)

    account = get_aws_account()

    create_plot(name='AWS Costing ({})'.format(account), data=simplified_daily_usage)
    logger.info('Successfully completed Costing Report.')


#######################################
### Generic Functions #################
#######################################

def log_json(value, message=None):
    '''
    This is a function to log json
    '''
    if not message: message='{}'

    if not isinstance(value, (dict, list)):
        logger.fatal('Invalid object.')
        raise TypeError("The provided value must be a dict or list.")

    log_value = message.format(
        json.dumps(
            value,
            indent=4
        )
    )

    logger.info(log_value)

    return log_value


def get_aws_account(iam=iam, sts=sts):
    aws_account_number = sts.get_caller_identity().get('Account')

    try:
        aws_account = iam.list_account_aliases()["AccountAliases"][0]
    except IndexError as e:
        logger.info('No account alias seems to be set: {}'.format(e))
        aws_account = aws_account_number

    return aws_account


#######################################
### Program Specific Functions ########
#######################################

def get_time_period(past=TIME_PERIOD_IN_DAYS):
    '''
    Generate time_period json based on past number of days for cost
    explorer API
    '''
    if not isinstance(past, int):
        raise TypeError("Time period must be specified as an int.")

    today       = date.today()
    yesterday   = today - timedelta(days=1)
    x_days_ago  = yesterday - timedelta(days=past)

    time_period = {
        'Start': '{}'.format(x_days_ago),
        'End': '{}'.format(yesterday)
    }

    log_json(value=time_period, message="Checking the following time period: {}")

    return time_period


def create_plot(name, data, x_key='date', y_key='ammount', color=['#4285F4', '#f48942']):
    if len(data) == 0:
        raise ValueError
    elif len(data) > 0:
        x = [
            d[x_key] for d in data
        ]

        y = [
            d[y_key] for d in data
        ]

        graph.figure(figsize=(len(data), 8), dpi=320)
        graph.bar(x, y, width=0.75, color=color)

        graph.xlabel(x_key)
        graph.ylabel(y_key)

        x_labels = [
            '${:.2f}'.format(spent) for spent in y
        ]

        vertical_offset = 0.02 * max(y)

        logger.info(vertical_offset)

        for i in range(len(data)):
            graph.text(x[i], y[i] + vertical_offset, x_labels[i], horizontalalignment='center')

        graph.title('{} last {} days'.format(name, len(data)))

        graph.savefig('{}.png'.format(name))

        return graph


def get_simplified_daily_usage(time_period):
    daily_usage = cost_explorer.get_cost_and_usage(
        TimePeriod=time_period,
        Granularity='DAILY',
        Metrics=['BlendedCost']
    )['ResultsByTime']

    logger.debug('Daily Usage: {}'.format(json.dumps(daily_usage, indent=4)))

    simplified_daily_usage = [
        {
            "date": re.sub(
                r'\d{4}-',
                '',
                daily["TimePeriod"]["Start"]
            ),
            "ammount": float(daily["Total"]["BlendedCost"]["Amount"])
        } for daily in daily_usage
    ]

    log_json(value=simplified_daily_usage, message="Simplified Usage: {}")

    return simplified_daily_usage


#######################################
### Execution #########################
#######################################

def lambda_handler(event, context):
    main(event, context)


if __name__ == '__main__':
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    event = {
        "event": "Something Witty."
    }

    main(event, None)
